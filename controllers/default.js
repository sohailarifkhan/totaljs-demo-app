exports.install = function () {
	F.route('/', readfromflow);
	F.route('/temperatureLimit', tempertatureLimitFunction);
	F.route('/square', calculateSquare);
};

function readfromflow() {
	var self = this;
	// FLOW is a global variable defined in FLOW package
	var instance = FLOW.findByReference('temperatureLimit')[0];
	if (!instance)
		return this.throw404();

	instance.emit('data', self);


}

function tempertatureLimitFunction() {
	var instance = FLOW.findByReference('temperatureLimit')[0];
	if (!instance)
		return this.throw404();
	var self = this;
	instance.emit('temperatureLimit', self);
}

function calculateSquare() {
	var instance = FLOW.findByReference('square')[0];
	if (!instance)
		return this.throw404();
	var self = this;
	instance.emit('square', self);
}