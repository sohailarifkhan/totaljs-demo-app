exports.id = 'sqaure';
exports.title = 'Square';
exports.group = 'Common';
exports.color = '#2d4a78';
exports.icon = 'code';
exports.input = true;
exports.output = 1;
exports.version = '1.0.0';
exports.author = 'Martin Smola';
exports.options = {
    outputs: 1,
    code: `send('Hello world!');`};


exports.install = function(instance) {

    var fn;
    var ready = false;

    var VALUE = {
        instance: {
            get: instance.get.bind(instance),
            set: instance.set.bind(instance),
            rem: instance.rem.bind(instance),
            error: instance.error.bind(instance),
            debug: instance.debug.bind(instance),
            status: instance.status.bind(instance),
            send: function(flowdata, index, data){

                if (!data){
                    if (!index)
                        return instance.send2(flowdata.clone());
                    data = index;
                    index = 0;
                }

                flowdata = flowdata.clone();
                flowdata.data = data;
                instance.send2(index, flowdata);
            }
        },
        global: {
            get: FLOW.get,
            set: FLOW.set,
            rem: FLOW.rem,
            variable: FLOW.variable
        },
        Date: Date,
        Object: Object
    };

    instance.custom.reconfigure = function(){
        fn = SCRIPT(`
			var instance = value.instance;
			var flowdata = value.flowdata;
			var Date = value.Date;
			var Object = value.Object;
			var global = value.global;
			var send = function(index, data){
				value.instance.send(value.flowdata, index, data);
			}
			${instance.options.code}
			next(value);
		`);

        if (typeof(fn) !== 'function') {
            ready = false;
            instance.error(fn.message);
            return;
        }
        ready = true;
    };

    instance.on('data', function(flowdata) {
        console.log(flowdata);
        VALUE.flowdata = flowdata;

        ready && fn(VALUE, function(err) {
            if (err)
                return instance.data('Error while processing function ' + flowdata);
        });
        return flowdata;
    });
    instance.on('sendValue', (self) => {
        self.plain('current', true);
        console.log(self.req);
        instance.send2(self);
    });

    instance.on('square', (self) => {
        let temp = self.req.headers['number'];
            self.json({"square":temp*temp}, true);
        instance.send2(temp*temp);
    });


    instance.on('options', instance.custom.reconfigure);
    instance.custom.reconfigure();
};