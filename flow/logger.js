exports.id = 'logger';
exports.title = 'Logger';
exports.group = 'Common';
exports.color = '#0b785e';
exports.icon = 'code';
exports.input = true;
exports.output = 1;
exports.version = '1.0.0';
exports.author = 'Martin Smola';
exports.options = {
    outputs: 1,
    code: `send('Hello world!');`};


exports.install = function(instance) {

    var fn;
    var ready = false;

// on receving some data.
    instance.on('data', function(flowdata) {
        console.log('--------------Logging Data--------------------');
        console.log(flowdata.data);
        console.log('----------------------------------------------');
    });
// on options provided
   // instance.on('options', instance.custom.reconfigure);
   // instance.custom.reconfigure();
};