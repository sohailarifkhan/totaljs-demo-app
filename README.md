# README #

This README would give you idea, How to run a totaljs demo application.

### What is this repository for? ###

* Repository is for demo application created using totaljs

### How do I get set up? ###

* Run ```npm install ``` to install dependencies .
* Run ``` npm start ``` to run application.
* You can view flow UI in browser. for example if you are running codeon local mechine you can view flow UI on link ```http://localhost:4500/$flow```

### Usage ###
* You call temperatureLimit endpoint and send temperature in header to check the limit. it will return json providing result.
* You can call square endpoint and provide number in header. You will get the square.
### Contribution guidelines ###

* new components can be addded to component directory. Include them using flow UI.

